package com.hitesh.recyclerviewsample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView programmingList = (RecyclerView) findViewById(R.id.recyclerList);
        programmingList.setLayoutManager(new LinearLayoutManager(this));
        String[] items= {"ITEM1", "ITEM2", "ITEM3", "ITEM5", "ITEM5", "ITEM6"};
        programmingList.setAdapter(new RecyclerViewAdapter(items));
    }
}
